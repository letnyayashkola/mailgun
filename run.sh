#!/bin/sh

mkdir -p /var/run

gunicorn -w $(nproc) --bind unix:/var/run/mailgun.sock wsgi:app
