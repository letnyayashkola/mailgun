FROM python:3.8-alpine as builder

RUN apk update && apk add curl

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=on \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    PATH=/etc/poetry/bin:$PATH \
    POETRY_HOME=/etc/poetry \
    PURL=https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py

RUN curl -sSL $PURL | python

WORKDIR /app

COPY poetry.lock .
COPY pyproject.toml .

RUN poetry config virtualenvs.in-project true \
    && poetry install --no-root --no-dev --no-interaction

FROM python:3.8-alpine

WORKDIR /app

COPY --from=builder /app/.venv /app/.venv
COPY . .

ENV PATH=/app/.venv/bin:$PATH

ENTRYPOINT [ "./run.sh" ]
