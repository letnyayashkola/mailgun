#!/bin/sh

if [ -f ".env" ]; then
    echo ".env file alredy exists"
    exit 1
fi

cat > .env <<- EOF
DEBUG=1
FLASK_ENV=development
MAILGUN_API_KEY=1
EOF
