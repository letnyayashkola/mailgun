import logging
import os
from datetime import datetime

from flask import Flask, request

import requests
from requests import HTTPError

from .settings import MAILGUN_DOMAIN, REPLY_ADDRESS, REPLY_MSG_TEXT

app = Flask(__name__)

LOG = logging.getLogger(__name__)


def send_message(msg: dict):
    resp = requests.post(
        f"https://api.eu.mailgun.net/v3/{MAILGUN_DOMAIN}/messages",
        auth=("api", os.getenv("MAILGUN_API_KEY")),
        data={
            "from": f"Летняя школа <no-reply@{MAILGUN_DOMAIN}>",
            "subject": "Re: "+msg["subject"],
            "to": [msg["from"]],
            "h:Reply-To": REPLY_ADDRESS,
            "h:In-Reply-To:": msg["Message-Id"],
            "html": REPLY_MSG_TEXT.format(
                datetime.now().strftime('%d/%m/%Y %H:%M'),
                msg['from'],
                msg['Date'],
                msg['subject'],
                msg['body-plain'].replace("\n", "<br>")
            )
        })

    resp.raise_for_status()

    return resp.json()


@app.route('/', methods=["POST"])
def incoming_email():

    LOG.debug(request.form.keys())

    LOG.info("New message from %s", request.form["from"])

    try:
        resp = send_message(request.form)
        LOG.debug(resp)
    except HTTPError as e:
        LOG.error(e.response)
        LOG.error(e.response.content)

    LOG.info("Message from %s processed", request.form["from"])

    return b'', 200
