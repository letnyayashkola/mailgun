import base64
import os


REPLY_ADDRESS = "window@letnyayashkola.org"
MAILGUN_DOMAIN = "mg.letnyayashkola.org"

with open(os.path.join(os.getcwd(), "mailgun", "meme.png"), "rb") as file_:
    _image_base64 = base64.b64encode(file_.read()).decode('utf-8')


REPLY_MSG_TEXT = f"""
Привет! Я робот!
<br>
<br>
В этот почтовый ящик люди не смотрят.
<br>
Пиши им на <a href="mailto:{REPLY_ADDRESS}">{REPLY_ADDRESS}</a>
(можно просто ответить на это письмо) или сразу своей мастерской (так будет быстрей).
<br>
<img title="Если вы поняли этот мем, очень странно, что вы попали сюда"
src="data:image/png;base64,{_image_base64}"
alt="Тут должен был быть смешной мем">
<br>
<br>
С уваженимем, полковник!
<br>
<br>
<br>
<br>
<hr>
{{0}} от {{1}} я получил:
<br>
<hr>
<b>From:</b> {{1}}
<br>
<b>Date:</b> {{2}}
<br>
<b>Subject:</b> {{3}}
<br>
<br>
{{4}}
"""
