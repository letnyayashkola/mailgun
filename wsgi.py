import logging
import os

from dotenv import load_dotenv

from mailgun.app import app


load_dotenv()


logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=(logging.DEBUG if os.getenv("DEBUG") else logging.INFO)
)


if __name__ == "__main__":
    app.run(
        host='0.0.0.0', port=8000,
        debug=os.getenv("DEBUG", False),
    )  # noqa: S201
